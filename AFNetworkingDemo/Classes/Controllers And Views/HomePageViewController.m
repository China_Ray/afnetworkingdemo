//
//  HomePageViewController.m
//  AFNetworkingDemo
//
//  Created by yulei on 2017/8/24.
//  Copyright © 2017年 yulei. All rights reserved.
//

#import "HomePageViewController.h"
#import "VideoViewController.h"

@interface HomePageViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *sourceData;

@end

@implementation HomePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    //加载tableView数据
    [self loadTableViewData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews

- (NSArray *)tableViewCellsTitle {
    return @[@"测试网络连接", @"提示框 - 显示loading信息", @"提示框 - 只显示只文字信息", @"提示框 - 显示成功信息", @"提示框 - 显示失败信息", @"全屏播放视频"];
}

//加载tableView数据
- (void)loadTableViewData {
    //title
    self.navigationItem.title = @"功能列表";
    
    //source data
    _sourceData = [self tableViewCellsTitle];
    
    //fix margin-left:15px
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutManager:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - MPMoviePlayerViewController

//视频播放
- (void)videoPlayWiteURL:(NSString *)url {
    VideoViewController *videoViewController = [[VideoViewController alloc] init];
    videoViewController.videoURL = url;
    [self.navigationController pushViewController:videoViewController animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _sourceData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomePageTableViewCell"];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomePageTableViewCell"];
    cell.textLabel.text = _sourceData[indexPath.row];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"select row is %ld", (long)indexPath.row);
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    switch (indexPath.row) {
        case 0:
        {
            [NetworkHTTPManager GET:@"http://p2.zgirls.net/test.php"
                         parameters:nil
                            success:^(id responseObject) {
                                APPLog(@"%@", responseObject);
                            }
                            failure:^(NSError *error) {
                                APPLog(@"%@", error.description);
                            }];
            break;
        }
        case 1:
        {
            [OwnProgressHUD showLoading:@"数据加载中，请稍后..." inView:self.view];
            [OwnProgressHUD hideWithView:self.view afterDelay:2.0f];
            break;
        }
        case 2:
        {
            [OwnProgressHUD showOnlyTextMessage:@"数据加载中..." afterDelay:2.0f];
            break;
        }
        case 3:
        {
            [OwnProgressHUD showSuccessMessage:@"数据加载成功" afterDelay:2.0f];
            break;
        }
        case 4:
        {
            [OwnProgressHUD showErrorMessage:@"数据加载失败" afterDelay:2.0f];
            break;
        }
        case 5:
        {
            //http://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear2/prog_index.m3u8
            //https://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8
            [self videoPlayWiteURL:@"https://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8"];
            break;
        }
        default:
            break;
    }
}

@end
