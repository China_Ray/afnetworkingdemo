//
//  VideoPlayerView.h
//  AFNetworkingDemo
//  功能介绍 - 视频播放视图
//  Created by yulei on 2017/8/29.
//  Copyright © 2017年 yulei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoPlayerView : UIView

//设置视频播放地址
- (void)VideoPlayerWithURL:(NSString *)url;
//播放
- (void)play;
//停止
- (void)pause;
//移除
- (void)remove;

@end
