//
//  VideoViewController.m
//  AFNetworkingDemo
//
//  Created by yulei on 2017/8/25.
//  Copyright © 2017年 yulei. All rights reserved.
//

#import "VideoViewController.h"
#import "VideoPlayerView.h"

@interface VideoViewController ()

@property (nonatomic, strong) VideoPlayerView *playerView;

@end

@implementation VideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
 
    [self loadVideoPlayerView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeVideoPlayerView];
}

#pragma mark - Layout Subviews

//加载视频播放视图
- (void)loadVideoPlayerView {
    if (_videoURL) {
        _playerView = [[VideoPlayerView alloc] initWithFrame:self.view.bounds];
        [_playerView VideoPlayerWithURL:_videoURL];
        [self.view addSubview:_playerView];
    }
}

//移除视频播放视图
- (void)removeVideoPlayerView {
    if (_playerView) {
        [_playerView remove];
        [_playerView removeFromSuperview];
        _playerView = nil;
    }
}

@end
