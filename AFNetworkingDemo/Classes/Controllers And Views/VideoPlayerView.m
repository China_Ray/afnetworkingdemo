//
//  VideoPlayerView.m
//  AFNetworkingDemo
//
//  Created by yulei on 2017/8/29.
//  Copyright © 2017年 yulei. All rights reserved.
//

#import "VideoPlayerView.h"

@import AVFoundation;

@interface VideoPlayerView ()
//AVPlayerLayer 显示视频，AVPlayerItem 提供视频信息， AVPlayer 管理和调控。
@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVPlayerLayer *playerLayer;

@end

@implementation VideoPlayerView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

#pragma mark - Private Method

//Init AVPlayerItem
- (AVPlayerItem *)createPlayerItemWithURL:(NSString *)url {
    if (_playerItem) {
        return _playerItem;
    }
    
    NSURL *resourceURL = [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:resourceURL];
    _playerItem = playerItem;
    //添加监听
    //资源状态
    [_playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    //资源尺寸
    [_playerItem addObserver:self forKeyPath:@"presentationSize" options:NSKeyValueObservingOptionNew context:nil];
    //资源下载进度
    [_playerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionNew context:nil];
    //缓冲数据的状态(空和完成)
    [_playerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
    [_playerItem addObserver:self forKeyPath:@"playbackBufferFull" options:NSKeyValueObservingOptionNew context:nil];
    //缓冲可达到播放的状态
    [_playerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew context:nil];
    return _playerItem;
}

//Init AVPlayer
- (AVPlayer *)createPlayerWithURL:(NSString *)url {
    if (_player) {
        return _player;
    }
    
    AVPlayerItem *playerItem = [self createPlayerItemWithURL:url];
    AVPlayer *player = [AVPlayer playerWithPlayerItem:playerItem];
    player.automaticallyWaitsToMinimizeStalling = NO;
    
    _player = player;
    return _player;
}

//Init AVPlayerLayer
- (AVPlayerLayer *)createPlayerLayerWithURL:(NSString *)url {
    if (_playerLayer) {
        return _playerLayer;
    }
    
    AVPlayer *player = [self createPlayerWithURL:url];
    AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
    playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    
    //当播放进度改变的时候回主动回调该Block，但是当视频卡顿的时候是不会回调的，可以在该回调里面处理进度条以及播放时间的刷新
//    [_playerLayer addPeriodicTimeObserverForInterval:<#(CMTime)#> queue:<#(nullable dispatch_queue_t)#> usingBlock:<#^(CMTime time)block#>];
    
    _playerLayer = playerLayer;
    return _playerLayer;
}

//视频总时间
- (NSTimeInterval)videoDuration {
    return _playerItem? CMTimeGetSeconds(_playerItem.duration): 0;
}

//视频缓冲进度
- (NSTimeInterval)videoLoadedTimeRanges {
    //缓冲总进度
    NSTimeInterval totalRange= 0;
    if (_playerItem) {
        //资源下载进度
        NSArray *loadedTimeRanges = _playerItem.loadedTimeRanges;
        //缓冲区域
        CMTimeRange timeRange = [loadedTimeRanges.firstObject CMTimeRangeValue];
        
        NSTimeInterval startRange = CMTimeGetSeconds(timeRange.start);
        NSTimeInterval durationRange = CMTimeGetSeconds(timeRange.duration);
        totalRange = startRange + durationRange;
    }
    return totalRange;
}

//获取指定时间的显示格式(小时:分钟:秒)
- (NSString *)videoTimeString:(NSTimeInterval)time {
    NSInteger hour = time / 3600;
    NSInteger minutes = (NSInteger)time % 3600 / 60;
    NSInteger seconds = (NSInteger)time % 60;
    return hour? [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hour, (long)minutes, (long)seconds]: [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

#pragma mark - Public Method

//设置视频播放地址
- (void)VideoPlayerWithURL:(NSString *)url {
    if (!url) {
        return ;
    }

    _playerLayer = [self createPlayerLayerWithURL:url];
    _playerLayer.frame = self.bounds;
    [self.layer addSublayer:_playerLayer];
    return ;
}

//播放
- (void)play{
    
}

//停止
- (void)pause{
    
}

//移除
- (void)remove {
    APPLog(@"remove avplayer");
    if (_playerItem) {
        //移除监听
        //资源状态
        [_playerItem removeObserver:self forKeyPath:@"status" context:nil];
        //资源尺寸
        [_playerItem removeObserver:self forKeyPath:@"presentationSize" context:nil];
        //资源下载进度
        [_playerItem removeObserver:self forKeyPath:@"loadedTimeRanges" context:nil];
        //缓冲数据的状态(空和完成)
        [_playerItem removeObserver:self forKeyPath:@"playbackBufferEmpty" context:nil];
        [_playerItem removeObserver:self forKeyPath:@"playbackBufferFull" context:nil];
        //缓冲可达到播放的状态
        [_playerItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp" context:nil];  
    }
    
    [_player pause];
    [_player.currentItem cancelPendingSeeks];
    [_player.currentItem.asset cancelLoading];
    [_playerLayer removeFromSuperlayer];
    return ;
}

#pragma mark - Observer Value

//监听消息
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"status"]) {
        //资源状态
        AVPlayerItemStatus status = _playerItem.status;
        switch (status) {
            case AVPlayerItemStatusReadyToPlay:
            {
                APPLog(@"AVPlayerItemStatusReadyToPlay");
                break;
            }
            case AVPlayerItemStatusFailed:
            {
                APPLog(@"AVPlayerItemStatusFailed");
                break;
            }
            case AVPlayerItemStatusUnknown:
            {
                APPLog(@"AVPlayerItemStatusUnknown");
                APPLog(@"%@",_playerItem.error.description);
                break;
            }
            default:
                break;
        }
    } else if ([keyPath isEqualToString:@"presentationSize"]) {
        //资源尺寸
        APPLog(@"%@", NSStringFromCGSize(_playerItem.presentationSize));
    } else if ([keyPath isEqualToString:@"loadedTimeRanges"]) {
//        //缓冲总进度
//        NSTimeInterval totalRange = [self videoLoadedTimeRanges];
//        //进度
//        NSTimeInterval duration = [self videoDuration];
//        
//        CGFloat progress = totalRange / duration;
//        
//        APPLog(@"%@", [self videoTimeString:duration]);
//        APPLog(@"loadedTimeRanges totalRange is %f, duration is %f, progress is %f", totalRange, duration, progress);
    } else if ([keyPath isEqualToString:@"playbackBufferEmpty"]) {
        //缓冲数据为空
        APPLog(@"playbackBufferEmpty");
    } else if ([keyPath isEqualToString:@"playbackBufferFull"]) {
        //缓冲数据完毕
        APPLog(@"playbackBufferFull");
    } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        //缓冲可达到播放的状态
        APPLog(@"playbackLikelyToKeepUp");
    }
    return ;
}

@end
