//
//  VideoViewController.h
//  AFNetworkingDemo
//
//  Created by yulei on 2017/8/25.
//  Copyright © 2017年 yulei. All rights reserved.
//

#import <AVKit/AVKit.h>

typedef void(^AVPlayerStatusBlock)();

@interface VideoViewController : UIViewController

//视频地址
@property (nonatomic, copy) NSString *videoURL;

@end
