//
//  NetworkHTTPManager.m
//  AFNetworkingDemo
//
//  Created by yulei on 15/4/23.
//  Copyright (c) 2015年 yulei. All rights reserved.
//

#import "NetworkHTTPManager.h"
#import "AFNetworking.h"

//网络测试地址
#define DebugNetworkUrl @"https://www.baidu.com/"
//网络正式地址
#define ReleaseNetworkUrl @"https://www.baidu.com/"

static const NSTimeInterval kTimeOutInterval = 30;

@implementation NetworkHTTPManager

#pragma mark - 网络基本设置

+ (AFHTTPSessionManager *)shardManager {
    AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
    
    [sessionManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain", nil]];
    [sessionManager.requestSerializer setTimeoutInterval:kTimeOutInterval];
    return sessionManager;
}

#pragma mark - HTTP网络请求

//get方法
+ (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(NSDictionary *)parameters
                      success:(NetworkRequestCompletionBlock)success
                      failure:(NetworkRequestErrorBlock)failure {
    
    AFHTTPSessionManager *manager = [self shardManager];
    
    NSString *urlString = [URLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLSessionDataTask *dataTask = [manager GET:urlString
                                       parameters:parameters
                                         progress:nil
                                          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 success(responseObject);
                                             });
                                         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 failure(error);
                                             });
                                         }];
    return dataTask;
}

//post方法
+ (NSURLSessionDataTask *)POST:(NSString *)URLString
                sessionManager:(AFHTTPSessionManager*)manager
                    parameters:(NSDictionary *)parameters
                       success:(NetworkRequestCompletionBlock)success
                       failure:(NetworkRequestErrorBlock)failure {

    NSString *urlString = [URLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLSessionDataTask *dataTask = [manager POST:urlString
                                        parameters:parameters
                                          progress:nil
                                           success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   success(responseObject);
                                               });
                                           }
                                           failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   failure(error);
                                               });
                                           }];
    return dataTask;
}

//post方法，默认30秒后超时
+ (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(NSDictionary *)parameters
                       success:(NetworkRequestCompletionBlock)success
                       failure:(NetworkRequestErrorBlock)failure {
    
    AFHTTPSessionManager *sessionManager = [self shardManager];
    return [self POST:URLString sessionManager:sessionManager parameters:parameters success:success failure:failure];
}

//post方法，超时操作
+ (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(NSDictionary *)parameters
               timeoutInterval:(NSTimeInterval)timeout
                       success:(NetworkRequestCompletionBlock)success
                       failure:(NetworkRequestErrorBlock)failure {
    
    AFHTTPSessionManager *sessionManager = [self shardManager];
    [sessionManager.requestSerializer setTimeoutInterval:timeout];
    return [self POST:URLString sessionManager:sessionManager parameters:parameters success:success failure:failure];
}

@end
