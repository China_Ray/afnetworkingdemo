//
//  NetworkClientManager.h
//  AFNetworkingDemo
//  功能描述 - 网络接口封装
//  Created by yulei on 15/4/23.
//  Copyright (c) 2015年 yulei. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^NetworkRequestCompletionBlock)(id responseObject);
typedef void(^NetworkRequestErrorBlock)(NSError *error);

@interface NetworkHTTPManager : NSObject

#pragma mark - HTTP网络请求
/*
 函数注释 : 基本http请求，目前只支持post和get
 URLString : 传递请求地址，目前传递整个url地址，如果后期分布式另议
 Parameters : 传递数据，以字典方式传递key-value
 TimeoutInterval : 超时时间
 Success : block，成功返回请求数据
 Failure : block，失败返回错误信息
 */
//get方法
+ (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(NSDictionary *)parameters
                      success:(NetworkRequestCompletionBlock)success
                      failure:(NetworkRequestErrorBlock)failure;
//post方法，默认30秒后超时
+ (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(NSDictionary *)parameters
                       success:(NetworkRequestCompletionBlock)success
                       failure:(NetworkRequestErrorBlock)failure;
//post方法，超时操作
+ (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(NSDictionary *)parameters
               timeoutInterval:(NSTimeInterval)timeout
                       success:(NetworkRequestCompletionBlock)success
                       failure:(NetworkRequestErrorBlock)failure;

@end
