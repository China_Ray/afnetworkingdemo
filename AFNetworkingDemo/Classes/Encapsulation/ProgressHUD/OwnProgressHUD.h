//
//  OwnProgressHUD.h
//  AFNetworkingDemo
//  功能描述 - 进度提示封装
//  Created by yulei on 2017/8/17.
//  Copyright © 2017年 yulei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnProgressHUD : NSObject

/**
 显示等待提示，需要调用隐藏方法
 
 @param text 文字信息
 @param view 被添加的视图
 @return instancetype
 */
+ (void)showLoading:(NSString *)text;
+ (void)showLoading:(NSString *)text inView:(UIView *)view;

/**
 只显示文字信息提示
 
 @param text 文字信息
 @param view 被添加的视图
 @param delay 延迟秒隐藏
 */
+ (void)showOnlyTextMessage:(NSString *)text;
+ (void)showOnlyTextMessage:(NSString *)text afterDelay:(NSTimeInterval)delay;
+ (void)showOnlyTextMessage:(NSString *)text inView:(UIView *)view;
+ (void)showOnlyTextMessage:(NSString *)text inView:(UIView *)view afterDelay:(NSTimeInterval)delay;

/**
 显示成功状态的信息提示
 
 @param text 文字信息
 @param view 被添加的视图
 @param delay 延迟秒隐藏
 */
+ (void)showSuccessMessage:(NSString *)text;
+ (void)showSuccessMessage:(NSString *)text afterDelay:(NSTimeInterval)delay;
+ (void)showSuccessMessage:(NSString *)text inView:(UIView *)view;
+ (void)showSuccessMessage:(NSString *)text inView:(UIView *)view afterDelay:(NSTimeInterval)delay;

/**
 显示失败状态的信息提示
 
 @param text 文字信息
 @param view 被添加的视图
 @param delay 延迟秒隐藏
 */
+ (void)showErrorMessage:(NSString *)text;
+ (void)showErrorMessage:(NSString *)text afterDelay:(NSTimeInterval)delay;
+ (void)showErrorMessage:(NSString *)text inView:(UIView *)view;
+ (void)showErrorMessage:(NSString *)text inView:(UIView *)view afterDelay:(NSTimeInterval)delay;

/**
 延迟隐藏
 
 @param view 被添加的视图
 @param delay 延迟秒隐藏
 */
+ (void)hide;
+ (void)hideWithView:(UIView *)view;
+ (void)hideWithView:(UIView *)view afterDelay:(NSTimeInterval)delay;

@end
