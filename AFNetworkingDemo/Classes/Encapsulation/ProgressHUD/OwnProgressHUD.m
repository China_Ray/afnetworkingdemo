//
//  OwnProgressHUD.m
//  AFNetworkingDemo
//
//  Created by yulei on 2017/8/17.
//  Copyright © 2017年 yulei. All rights reserved.
//

#import "OwnProgressHUD.h"
#import "MBProgressHUD.h"

//默认隐藏时间
static const NSTimeInterval kAnimationHideDuration = 2.0f;

@interface OwnProgressHUD ()

//初始化视图
+ (MBProgressHUD *)createProgressHUDWithView:(UIView *)view;

//显示自定义图片的信息提示
+ (void)showCustomImage:(NSString *)imgName message:(NSString *)text inView:(UIView *)view afterDelay:(NSTimeInterval)delay;

@end

@implementation OwnProgressHUD

#pragma mark - private methods

/**
 初始化视图

 @param view 被添加的视图
 @return MBProgressHUD
 */
+ (MBProgressHUD *)createProgressHUDWithView:(UIView *)view {
    if (!view) {
        view = [UIApplication sharedApplication].keyWindow;
    }
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
    hud.backgroundColor = [UIColor clearColor];
    hud.bezelView.color = [UIColor blackColor];
    hud.contentColor = [UIColor whiteColor];
    hud.alpha = 0.75f;
    hud.margin = 20.0f;
    hud.minSize = CGSizeMake(100.0f, 80.0f);
    hud.animationType = MBProgressHUDAnimationZoom;
    hud.removeFromSuperViewOnHide = YES;
    [view addSubview:hud];
    return hud;
}

/**
 显示自定义图片的信息提示
 
 @param imgName 自定义图片名称
 @param text 文字信息
 @param view 被添加的视图
 @param delay 延迟秒隐藏
 */
+ (void)showCustomImage:(NSString *)imgName message:(NSString *)text inView:(UIView *)view afterDelay:(NSTimeInterval)delay {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [self createProgressHUDWithView:view];
        hud.mode = MBProgressHUDModeCustomView;
        hud.label.text = text;
        
        UIImage *customImg = [UIImage imageWithContentsOfBundle:@"ProgressHUD" File:imgName];
        UIImageView *customView = [[UIImageView alloc] initWithImage:customImg];
        hud.customView = customView;
        
        //注意循环引用
        __weak typeof(customView) weakCustomView = customView;
        hud.completionBlock = ^{
            __strong typeof(weakCustomView) strongCustomView = weakCustomView;
            [strongCustomView removeFromSuperview];
            strongCustomView = nil;
        };
        
        [hud showAnimated:YES];
        [hud hideAnimated:YES afterDelay:delay];
    });
    return ;
}

#pragma mark - MBProgressHUD Delegate
//备注：Animated为NO时调用，否则开启removeFromSuperViewOnHide，或手动移除
- (void)hudWasHidden:(MBProgressHUD *)hud {
    if (hud.delegate) {
        hud.delegate = nil;
    }
    
    [hud removeFromSuperview];
    hud = nil;
}

#pragma mark - public methods

/**
 显示等待提示，需要调用隐藏方法
 
 @param text 文字信息
 @param view 被添加的视图
 @return return OwnProgressHUD
 */
+ (void)showLoading:(NSString *)text {
    return [self showLoading:text inView:nil];
}

+ (void)showLoading:(NSString *)text inView:(UIView *)view {
    if (!view) {
        view = [UIApplication sharedApplication].keyWindow;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [self createProgressHUDWithView:view];
        hud.mode = MBProgressHUDModeCustomView;
        hud.label.text = text;

        UIImage *loadingImg = [UIImage imageWithContentsOfBundle:@"ProgressHUD" File:@"ProgressHUD_loading.png"];
        UIImageView *loadingView = [[UIImageView alloc] initWithImage:loadingImg];
        
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        animation.toValue = @(M_PI*2);
        animation.repeatCount = HUGE_VALF;
        animation.duration = 1.0f;
        [loadingView.layer addAnimation:animation forKey:nil];
        hud.customView = loadingView;
        
        //注意循环引用
        __weak typeof(loadingView) weakLoadingView = loadingView;
        hud.completionBlock = ^{
            __strong typeof(weakLoadingView) strongLoadingView = weakLoadingView;
            [strongLoadingView.layer removeAllAnimations];
            [strongLoadingView removeFromSuperview];
            strongLoadingView = nil;
        };
        
        [hud showAnimated:YES];
    });
    return ;
}

/**
 只显示文字信息提示
 
 @param text 文字信息
 @param view 被添加的视图
 @param delay 延迟秒隐藏
 */
+ (void)showOnlyTextMessage:(NSString *)text {
    return [self showOnlyTextMessage:text inView:nil afterDelay:kAnimationHideDuration];
}

+ (void)showOnlyTextMessage:(NSString *)text afterDelay:(NSTimeInterval)delay {
    return [self showOnlyTextMessage:text inView:nil afterDelay:delay];
}

+ (void)showOnlyTextMessage:(NSString *)text inView:(UIView *)view {
    return [self showOnlyTextMessage:text inView:view afterDelay:kAnimationHideDuration];
}

+ (void)showOnlyTextMessage:(NSString *)text inView:(UIView *)view afterDelay:(NSTimeInterval)delay {
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [self createProgressHUDWithView:view];
        hud.mode = MBProgressHUDModeText;
        hud.label.text = text;
        [hud showAnimated:YES];
        [hud hideAnimated:YES afterDelay:kAnimationHideDuration];
    });
    return ;
}

/**
 显示成功状态的信息提示
 
 @param text 文字信息
 @param view 被添加的视图
 @param delay 延迟秒隐藏
 */
+ (void)showSuccessMessage:(NSString *)text {
    return [self showSuccessMessage:text inView:nil afterDelay:kAnimationHideDuration];
}

+ (void)showSuccessMessage:(NSString *)text afterDelay:(NSTimeInterval)delay {
    return [self showSuccessMessage:text inView:nil afterDelay:delay];
}

+ (void)showSuccessMessage:(NSString *)text inView:(UIView *)view {
    return [self showSuccessMessage:text inView:view afterDelay:kAnimationHideDuration];
}

+ (void)showSuccessMessage:(NSString *)text inView:(UIView *)view afterDelay:(NSTimeInterval)delay {
    return [self showCustomImage:@"ProgressHUD_success.png" message:text inView:view afterDelay:delay];
}

/**
 显示失败状态的信息提示
 
 @param text 文字信息
 @param view 被添加的视图
 @param delay 延迟秒隐藏
 */
+ (void)showErrorMessage:(NSString *)text {
    return [self showErrorMessage:text inView:nil afterDelay:kAnimationHideDuration];
}

+ (void)showErrorMessage:(NSString *)text afterDelay:(NSTimeInterval)delay {
    return [self showErrorMessage:text inView:nil afterDelay:delay];
}

+ (void)showErrorMessage:(NSString *)text inView:(UIView *)view {
    return [self showErrorMessage:text inView:nil afterDelay:kAnimationHideDuration];
}

+ (void)showErrorMessage:(NSString *)text inView:(UIView *)view afterDelay:(NSTimeInterval)delay {
    return [self showCustomImage:@"ProgressHUD_fail.png" message:text inView:view afterDelay:delay];
}

/**
 延迟隐藏
 
 @param view 被添加的视图
 @param delay 延迟秒隐藏
 */
+ (void)hide {
    return [self hideWithView:nil afterDelay:0.1];
}

+ (void)hideWithView:(UIView *)view {
    return [self hideWithView:view afterDelay:kAnimationHideDuration];
}

+ (void)hideWithView:(UIView *)view afterDelay:(NSTimeInterval)delay {
    if (!view) {
        view = [UIApplication sharedApplication].keyWindow;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [MBProgressHUD HUDForView:view];
        if (hud) {
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:YES afterDelay:delay];
        }
    });
}

@end
