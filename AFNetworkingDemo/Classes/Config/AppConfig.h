//
//  AppConfig.h
//  AFNetworkingDemo
//
//  Created by yulei on 2017/8/17.
//  Copyright © 2017年 yulei. All rights reserved.
//

#ifndef AppConfig_h
#define AppConfig_h

//系统版本
#define SystemVersion [[[UIDevice currentDevice] systemVersion] floatValue]
//设备模式
#define SystemModal [[UIDevice currentDevice] model]
//屏幕尺寸宽度
#define ScreenWidth [UIScreen mainScreen].bounds.size.width
//屏幕尺寸高度
#define ScreenHeight [UIScreen mainScreen].bounds.size.height

// 日志输出
#ifdef APPLOGDEBUG
  #define APPLog(format,...) NSLog((@"File:[%s]\nFunction:[%s]\nLine:[%d]:" format), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
  #define APPLog(...)
#endif

#endif /* AppConfig_h */
