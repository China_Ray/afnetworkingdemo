//
//  UIImage+Expand.m
//  AFNetworkingDemo
//
//  Created by yulei on 2017/8/21.
//  Copyright © 2017年 yulei. All rights reserved.
//

#import "UIImage+Expand.h"

@implementation UIImage (Expand)

//从bundle文件读取图片
+ (UIImage *)imageWithContentsOfBundle:(NSString *)path File:(NSString *)name {
    
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:path ofType:@"bundle"];
    if (bundlePath) {
        NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
        return [UIImage imageWithContentsOfFile:[bundle pathForResource:name ofType:nil]];
    }
    return nil;
}

@end
