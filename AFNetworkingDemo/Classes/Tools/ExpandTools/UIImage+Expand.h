//
//  UIImage+Expand.h
//  AFNetworkingDemo
//
//  Created by yulei on 2017/8/21.
//  Copyright © 2017年 yulei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Expand)

//从bundle文件读取图片
+ (UIImage *)imageWithContentsOfBundle:(NSString *)path File:(NSString *)name;

@end
